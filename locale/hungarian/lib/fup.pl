# config for F*EX CGI fup
# use utf8;

$info_1 = $info_login = <<EOD;
<p><hr><p>
<a href="/">F*EX (File EXchange)</a>
egy szolgáltatás nagy (hatalmas, óriási ...) file-ok küldésére.
<p>
A feladó (te) feltölti a filet a F*EX szerverre, a címzett pedig automatikusan megkapja
e-mailben a file letöltéséhez szükséges URL címet.<br>
A file letöltése után vagy $keep_default nap elmúltával a file automatikusan törlődik a szerverről.
A F*EX nem egy archívum!
<p>
Lásd még a <a href="/FAQ/">kérdések és válaszok oldalt</a> (angol) valamint
az egyéb <a href="http://fex.belwue.de/usecases/">felhasználási területeket</a> bemutató leírást.
<p><hr><p>
<address>
  <a href="mailto:$ENV{SERVER_ADMIN}">$ENV{SERVER_ADMIN}</a><br>
</address>
EOD

$info_2 = <<EOD;
<p><hr><p>
Beküldés után egy feltöltési folyamatjelző lesz látható
(ha a böngésződben a javasrcipt és a felugró ablakok engedélyezve vannak).
<p>
<em>MEGJEGYZÉS: Több böngésző nem támogatja a 2 GB-nál nagyobb file-ok feltöltését!</em><br>
Ennél nagyobb file küldéséhez egy speciális <a href="/tools.html">F*EX klienst</a> kell használni,
vagy a böngészők közül a Firefoxot vagy a Google Chrome-ot, melyeknek nincs méret korlátjuk. <br>
A <a href="/tools.html">F*EX kliens</a> használatára megszakadt feltöltések folytatásához is szükséged lehet. Ezt a böngészők nem tudják.
<p>
Több file küldéséhez azokat be kell csomagolni, például
egy <a href="http://www.7-zip.org/download.html">7-Zip</a> csomagoló programmal.
<p>
Lásd még a <a href="/FAQ/user.html">FAQ<a> (angol) listát és a
az egyéb <a href="http://fex.belwue.de/usecases/">felhasználási területeit</a> a programnak.
<p><hr><p>
<address>
  <a href="mailto:$ENV{SERVER_ADMIN}">$ENV{SERVER_ADMIN}</a><br>
</address>
EOD
